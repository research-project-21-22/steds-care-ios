//
//  CustomDecodingStrategy.swift
//  Steds Care
//
//  Created by Erik Bautista on 11/14/21.
//

import Foundation

protocol CustomDecodingStrategy {
    typealias DecodingStrategies = (
        dateDecodingStrategy: JSONDecoder.DateDecodingStrategy,
        dataDecodingStrategy: JSONDecoder.DataDecodingStrategy,
        nonConformingFloatDecodingStrategy: JSONDecoder.NonConformingFloatDecodingStrategy
    )
    static var decodingStrategies: (DecodingStrategies) { get }
}

protocol CustomEncodingStrategy {
    typealias EncodingStrategies = (
        dateEncodingStrategy: JSONEncoder.DateEncodingStrategy,
        dataEncodingStrategy: JSONEncoder.DataEncodingStrategy,
        nonConformingFloatDecodingStrategy: JSONEncoder.NonConformingFloatEncodingStrategy
    )
    static var encodingStrategies: (EncodingStrategies) { get }
}
